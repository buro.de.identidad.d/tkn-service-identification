package com.teknei.bid.service;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.teknei.bid.request.FimpeRequest;
import com.teknei.bid.response.FimpeResponse;
import com.teknei.bid.service.wsdl.INEVerificationService;
import com.teknei.bid.service.wsdl.INEVerificationService_Service;


@Service
public class Fimpe {

    private static final Logger log = LoggerFactory.getLogger(Fimpe.class);

	public FimpeResponse ineVerification(FimpeRequest fimpeRequest){
		try {
			// Creando request..
			String request = new JSONObject(fimpeRequest).toString();
			log.info("FIMPE REQUEST:" + request);
			// Llamando a servicio soap fimpe..
			INEVerificationService_Service ineVerificationService = new INEVerificationService_Service();
			INEVerificationService ineVerificationProxy = ineVerificationService.getINEVerificationService();	
			String result = ineVerificationProxy.verificaDatos(request);
			log.info("FIMPE RESPONSE:" + result);
			FimpeResponse response = mapper(result);
			//TODO persist
			return response;
		} catch (Exception e) {
			log.error("FIMPE exception", e.getMessage());
			return null;
		}
	}

	private FimpeResponse mapper(String json) {
		if (json == null)
			return null;
		JSONObject response = new JSONObject(json);
		FimpeResponse fimpeResponse = new FimpeResponse();
		fimpeResponse.setCodigoRespuesta(response.optString("codigoRespuesta"));
		fimpeResponse.setDescripcionRespuesta(response.optString("descripcionRespuesta"));
		fimpeResponse.setRespuestaServicio(response.optBoolean("respuestaServicio"));
		return fimpeResponse;
	}

}