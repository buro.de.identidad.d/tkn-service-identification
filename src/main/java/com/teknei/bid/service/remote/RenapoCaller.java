package com.teknei.bid.service.remote;

import com.teknei.bid.service.remote.configuration.BasicCurpConfig;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(url = "https://curp.nubarium.com", name = "renapoClient", configuration = BasicCurpConfig.class)
public interface RenapoCaller {

    @RequestMapping(method = RequestMethod.POST, value = "/renapo/valida_curp")
    String validateCurp(String request);

    @RequestMapping(method = RequestMethod.POST, value = "/renapo/obtener_curp")
    String getCurp(String request);

}