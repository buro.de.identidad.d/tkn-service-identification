
package com.teknei.bid.service.wsdl;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "INEVerificationService", targetNamespace = "services.middleware.fimpe.org.mx")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface INEVerificationService {


    /**
     * 
     * @param arg0
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "verificaDatos", targetNamespace = "services.middleware.fimpe.org.mx", className = "com.teknei.bid.wsdl.VerificaDatos")
    @ResponseWrapper(localName = "verificaDatosResponse", targetNamespace = "services.middleware.fimpe.org.mx", className = "com.teknei.bid.wsdl.VerificaDatosResponse")
    public String verificaDatos(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

}
