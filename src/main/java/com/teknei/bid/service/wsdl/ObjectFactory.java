
package com.teknei.bid.service.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.teknei.bid.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _VerificaDatosResponse_QNAME = new QName("services.middleware.fimpe.org.mx", "verificaDatosResponse");
    private final static QName _VerificaDatos_QNAME = new QName("services.middleware.fimpe.org.mx", "verificaDatos");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.teknei.bid.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VerificaDatos }
     * 
     */
    public VerificaDatos createVerificaDatos() {
        return new VerificaDatos();
    }

    /**
     * Create an instance of {@link VerificaDatosResponse }
     * 
     */
    public VerificaDatosResponse createVerificaDatosResponse() {
        return new VerificaDatosResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificaDatosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "services.middleware.fimpe.org.mx", name = "verificaDatosResponse")
    public JAXBElement<VerificaDatosResponse> createVerificaDatosResponse(VerificaDatosResponse value) {
        return new JAXBElement<VerificaDatosResponse>(_VerificaDatosResponse_QNAME, VerificaDatosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificaDatos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "services.middleware.fimpe.org.mx", name = "verificaDatos")
    public JAXBElement<VerificaDatos> createVerificaDatos(VerificaDatos value) {
        return new JAXBElement<VerificaDatos>(_VerificaDatos_QNAME, VerificaDatos.class, null, value);
    }

}
