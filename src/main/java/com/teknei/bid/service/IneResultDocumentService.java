package com.teknei.bid.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import com.teknei.bid.command.impl.credential.StoreTasAdditionalCredentialCommand;

@Service
public class IneResultDocumentService {

	private static final Logger log = LoggerFactory.getLogger(IneResultDocumentService.class);

	@Autowired
	private StoreTasAdditionalCredentialCommand storeTasAdditionalCredentialCommand;

	// ---------creacion de pdf ----->
	public void getInVerification(JSONObject jRequest, JSONObject jResponse, int count) { 
		Date date = new Date();
		DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String convertido = fechaHora.format(date);
		
		log.info("INFO: Generando reporte consulta ine");
		File file = null;
		try {
			file = File.createTempFile("temp", ".pdf");

			// 1. Create document
			Document document = new Document(PageSize.A4, 50, 50, 50, 50);
			// 2. Create PdfWriter
			PdfWriter.getInstance(document, new FileOutputStream(file));
			// 3. Open document
			document.open();
			// 4. Add content
			Font fontTitulos = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
			Font fontContentDescrptivo = FontFactory.getFont(FontFactory.HELVETICA, 9, BaseColor.BLACK);
			// Creacion del parrafo
			Paragraph paragraph0 = new Paragraph();
			// Agregar un titulo con su respectiva fuente
			paragraph0.add(new Phrase(" Documento de resultados de consulta al I.N.E ", fontTitulos));
			// Agregar saltos de linea
			paragraph0.add(new Phrase(Chunk.NEWLINE));
			paragraph0.add(new Phrase(Chunk.NEWLINE));
			paragraph0.setAlignment(Element.ALIGN_CENTER);
			paragraph0.setLeading(10);
			document.add(paragraph0);
			Paragraph paragraph1 = new Paragraph();
			// REQUEST
			paragraph1.add(new Phrase("Request ", fontTitulos));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("idOperation: " + jRequest.optString("idOperation"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("name: " + jRequest.optString("name"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1
					.add(new Phrase("fatherLastName: " + jRequest.optString("fatherLastName"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1
					.add(new Phrase("motherLastName: " + jRequest.optString("motherLastName"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("electorCode: " + jRequest.optString("electorCode"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("curp: " + jRequest.optString("curp"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("ocr: " + jRequest.optString("ocr"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("cic: " + jRequest.optString("cic"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1
					.add(new Phrase("emissionNumber: " + jRequest.optString("emissionNumber"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("emissionYear: " + jRequest.optString("emissionYear"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("registryYear: " + jRequest.optString("registryYear"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("city: " + jRequest.optString("city"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("state: " + jRequest.optString("state"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase("zipCode: " + jRequest.optString("zipCode"), fontContentDescrptivo));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			paragraph1.add(new Phrase(Chunk.NEWLINE));
			document.add(paragraph1);

			Paragraph paragraph2 = new Paragraph();
			// RESPONSE
			jResponse = jResponse.optJSONObject("response");
			paragraph2.add(new Phrase("Response ", fontTitulos));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("name: " + jResponse.optString("name"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2
					.add(new Phrase("fatherLastName: " + jResponse.optString("fatherLastName"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2
					.add(new Phrase("motherLastName: " + jResponse.optString("motherLastName"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("registryYear: " + jResponse.optString("registryYear"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("emissionYear: " + jResponse.optString("emissionYear"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2
					.add(new Phrase("emissionNumber: " + jResponse.optString("emissionNumber"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("electorCode: " + jResponse.optString("electorCode"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("curp: " + jResponse.optString("curp"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("ocr: " + jResponse.optString("ocr"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(
					new Phrase("stoleLostReport: " + jResponse.optString("stoleLostReport"), fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("registrationSituation: " + jResponse.optString("registrationSituation"),
					fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("matchFingerRigth: "
					+ jResponse.optJSONObject("additional").optJSONObject("biometrics").optString("matchFingerRigth"),
					fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("matchFingerLeft: "
					+ jResponse.optJSONObject("additional").optJSONObject("biometrics").optString("matchFingerLeft"),
					fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("idRequest: " + jResponse.optJSONObject("additional").optString("idRequest"),
					fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
//			paragraph2.add(new Phrase("register_date: " + jResponse.optString("registerDate"), fontContentDescrptivo));			
//			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("last_update_date: " + convertido, fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("intento: " + count, fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			paragraph2.add(new Phrase("errorCode: " + "00", fontContentDescrptivo));
			paragraph2.add(new Phrase(Chunk.NEWLINE));
			
			document.add(paragraph2);
			// 5. Close document
			document.close();
			byte[] fileContent = Files.readAllBytes(file.toPath());
			storeTasAdditionalCredentialCommand.addIneValidationToTas(fileContent, jRequest.optLong("idOperation"),count);
			

			log.info("INFO: El archivo "+file.getPath()+" ha sido guardado en el tas");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("ERROR: Error en datos: "+e.getMessage());
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			log.error("ERROR: Error al generar el documento: "+e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("ERROR: Error al guardar en el tas: "+e.getMessage());
		} finally {
			if (file.exists())
				file.delete();
		}
	}

}
