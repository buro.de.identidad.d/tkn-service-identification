package com.teknei.bid.service;

import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.service.remote.RenapoCaller;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class CurpService {

    @Autowired
    private BidClieRepository clieRepository;
    @Autowired
    private BidCurpRepository curpRepository;
    @Autowired
    private RenapoCaller renapoCaller;

    private static final DateTimeFormatter formatTarget = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private static final DateTimeFormatter formatSource = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public String getCurp(Long idClient, String entidad, Boolean document) {
        BidClie bidClie = clieRepository.findOne(idClient);
        String fchNac = bidClie.getFchNac();
        if (fchNac == null || fchNac.isEmpty()) {
            return null;
        }
        String gene = bidClie.getGeneClie();
        if (gene == null || gene.isEmpty()) {
            return null;
        }
        LocalDate dateNac = LocalDate.parse(fchNac, formatSource);
        String targetNac = dateNac.format(formatTarget);
        return getCurp(bidClie.getNomClie(), bidClie.getApePate(), bidClie.getApeMate(), targetNac, entidad, gene.toUpperCase().trim(), document);
    }

    public String getCurp(String name, String surnameFirst, String surnameLast, String fNac, String entidad, String gender, Boolean document) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("nombre", name);
        jsonObject.put("primerApellido", surnameFirst);
        jsonObject.put("segundoApellido", surnameLast);
        jsonObject.put("fechaNacimiento", fNac);
        jsonObject.put("entidad", entidad);
        jsonObject.put("sexo", gender);
        if(document){
            jsonObject.put("documento", "1");
        }
        String answer = renapoCaller.getCurp(jsonObject.toString());
        return answer;
    }

    public String validateCurp(String curp, Boolean document) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("curp", curp);
        if(document){
            jsonObject.put("documento", "1");
        }
        String answer = renapoCaller.validateCurp(jsonObject.toString());
        return answer;
    }

    public String validateCurp(Long idCustomer, Boolean document) {
        BidClieCurp curp = curpRepository.findTopByIdClie(idCustomer);
        if (curp == null) {
            return null;
        }
        return validateCurp(curp.getCurp(), document);
    }

}