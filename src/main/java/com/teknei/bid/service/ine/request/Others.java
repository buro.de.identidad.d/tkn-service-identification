package com.teknei.bid.service.ine.request;

import java.util.List;

import lombok.Data;

@Data
public class Others {
	private List<Biometric> bimetric;
	private Location LocationObject;
	private boolean consent;
}
