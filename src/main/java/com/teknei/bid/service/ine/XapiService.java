package com.teknei.bid.service.ine;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ObjectUtils.Null;
import org.jose4j.lang.JoseException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.teknei.bid.persistence.entities.CfgGeneralConfiguration;
import com.teknei.bid.persistence.repository.CfgGeneralConfigurationRepository;
import com.teknei.bid.service.ine.request.Biometric;
import com.teknei.bid.service.ine.request.Location;
import com.teknei.bid.service.ine.request.Others;
import com.teknei.bid.service.ine.request.ValidateIneRequest;

@Service
public class XapiService {

	private static final Logger log = LoggerFactory.getLogger(XapiService.class);

	@Autowired
	private CfgGeneralConfigurationRepository cfg;
	/* xapi-persons-identity Params-------------- */
	private String xapiPath;
	private String applicationid;
	private String service_channel;
	private String service_country;
	private String service_language;
	private String service_procedence;
	private String tracking_id;
	private String clientCertificate;
	private String clientIdP; // ", "ID de cliente");
	private String clientSecretP; // ", "Secreto de cliente");

	/* service-users/authenticate Params-------------- */
	private String tokenPath;
	private String tokenAplicationId; // ", "800");
	private String tokenServiceChannel; // ", "Movil");
	private String serviceCountry; // ", "MX");
	private String serviceLanguage; // ", "ES");
	private String serviceProcedence; // ", "Internet");
	private String clientId; // ", "ID de cliente");
	private String clientSecret; // ", "Secreto de cliente");
	private String tokenUser; // ", "Secreto de cliente");
	private String tokenPassword; // ", "Secreto de cliente");
	private String token;
	private String key;
	private String rsaData;

	String uuid;
//	 Carga de parametros de la base
	private void init() {
		log.info("INFO:  cargando configuraciones xapi ---->> ");
		List<CfgGeneralConfiguration> configList = cfg.findByCfgGroup("xapi");

		for (CfgGeneralConfiguration obj : configList) {
			log.info("INFO:  ---->> " + obj.getType() + ":" + obj.getType());
			if (obj.getType().equals("ine")) {
				if (obj.getParameter().equals("url")) {
					xapiPath = obj.getValue();
				}
				if (obj.getParameter().equals("application_id")) {
					applicationid = obj.getValue();
				}
				if (obj.getParameter().equals("service_channel")) {
					service_channel = obj.getValue();
				}
				if (obj.getParameter().equals("service_country")) {
					service_country = obj.getValue();
				}
				if (obj.getParameter().equals("service_language")) {
					service_language = obj.getValue();
				}
				if (obj.getParameter().equals("service_procedence")) {
					service_procedence = obj.getValue();
				}
				if (obj.getParameter().equals("tracking_id")) {
					tracking_id = obj.getValue();
				}
				if (obj.getParameter().equals("clientCertificate")) {
					clientCertificate = obj.getValue();
				}
				if (obj.getParameter().equals("client_id_p")) {
					clientIdP = obj.getValue();
				}
				if (obj.getParameter().equals("client_secret_p")) {
					clientSecretP = obj.getValue();
				}

			}
			// -----------------------------
			if (obj.getType().equals("token")) {
				if (obj.getParameter().equals("url")) {
					tokenPath = obj.getValue();
				}
				if (obj.getParameter().equals("application_id")) {
					tokenAplicationId = obj.getValue();
				}
				if (obj.getParameter().equals("service_channel")) {
					tokenServiceChannel = obj.getValue();
				}
				if (obj.getParameter().equals("service_country")) {
					serviceCountry = obj.getValue();
				}
				if (obj.getParameter().equals("service_language")) {
					serviceLanguage = obj.getValue();
				}
				if (obj.getParameter().equals("service_procedence")) {
					serviceProcedence = obj.getValue();
				}				
				if (obj.getParameter().equals("client_id")) {
					clientId = obj.getValue();
				}
				if (obj.getParameter().equals("client_secret")) {
					clientSecret = obj.getValue();
				}
				if (obj.getParameter().equals("tokenUser")) {
					tokenUser = obj.getValue();
				}
				if (obj.getParameter().equals("tokenPassword")) {
					tokenPassword = obj.getValue();
				}
				if (obj.getParameter().equals("rsa_data")) {
					rsaData = obj.getValue();
				}
			}

			if (obj.getType().equals("key")) {
				key = obj.getValue();
			}
		}

		CfgGeneralConfiguration tempToken = cfg.findByParameter("temp_token");
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		Date now = new Date(ts.getTime());
		Date registerDate = new Date(tempToken.getRegisterDate().getTime());

		if (now.after(registerDate)) {
			// generar nuevo token
			if (sendXampiToken().hasBody()) {
//					token = sendXampiToken().getBody();
				JSONObject json = new JSONObject(sendXampiToken().getBody());
				token = json.optString("access_token");
				tempToken.setValue(token);
				tempToken.setRegisterDate(addMinutes(15));
				cfg.save(tempToken);
			}
		} else {
			token = tempToken.getValue();
		}

		uuid = java.util.UUID.randomUUID().toString();
	}

	private Timestamp addMinutes(int amount) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, amount);
		return new Timestamp(calendar.getTime().getTime());
	}

	public ResponseEntity<String> sendXampi(String encriptBody) {
		
		log.info("sendXampi ");

		
		log.info("INFO:  llamando  a validacion ine ---->> " + uuid);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("AUTHORIZATION", token);
		headers.set("HEADER.APPLICATIONID", applicationid);// MOBAPP
		headers.set("HEADER.TRACKING_ID", tracking_id+"_"+uuid);// hj38ds8bhgrx8z6
		headers.set("HEADER.SERVICE_CHANNEL", service_channel);// IB3
		headers.set("HEADER.SERVICE_PROCEDENCE", service_procedence);// internet
		headers.set("HEADER.SERVICE_LANGUAGE", service_language); // ES
		headers.set("HEADER.SERVICE_COUNTRY", service_country);// MX
		headers.set("x-ibm-client-id", clientIdP);
		headers.set("x-ibm-client-secret", clientSecretP);
		headers.set("X-Client-Certificate", clientCertificate);
		JSONObject response = null;
		try {
			// Encrypt
			log.info("restTemplate ");
			log.info(xapiPath);
			HttpEntity<?> entity = new HttpEntity<>(encriptBody, headers);
//			HttpEntity<?> entity = new HttpEntity<>(body, headers);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> repo = restTemplate.exchange(xapiPath, HttpMethod.POST, entity, String.class);

			log.info("INFO: HEADERS:" + new JSONObject(repo.getHeaders()).toString());

			log.info("INFO: respuesta repo.getBody() validacion ine ---->> " + repo.getBody());
			EncryptAesUtil ecriptUtil = new EncryptAesUtil();
			String res = ecriptUtil.decryptJose(repo.getBody(), key);

			response = new JSONObject(res);
			JSONObject respo = response.getJSONObject("response");
			JSONObject curpValidation = new JSONObject();
			log.info("fatherLastNameMatch ");
			curpValidation.put("fatherLastNameMatch", respo.optBoolean("fatherLastName"));
			log.info("curpMatch ");
			curpValidation.put("curpMatch", respo.optBoolean("curp"));
			log.info("motherLastNameMatch ");
			curpValidation.put("motherLastNameMatch", respo.optBoolean("motherLastName"));
			log.info("nameMatch ");
			curpValidation.put("nameMatch", respo.optBoolean("name"));
			log.info("curpValidation ");
			respo.put("curpValidation", curpValidation);
			log.info("respo ");
			response.put("response", respo);
			log.info("INFO: respuesta response.toString() validacion ine ---->> " + response.toString());
			return new ResponseEntity<>(response.toString(), HttpStatus.OK);
		} catch (HttpClientErrorException k1) {
			log.error("Http code is not 2XX. The server responded: " + k1.getStatusCode() + " Cause: "
					+ k1.getResponseBodyAsString());
			EncryptAesUtil ecriptUtil = new EncryptAesUtil();
			String res = "";
			
			if (k1.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
				// actualiza token
				CfgGeneralConfiguration tempToken = cfg.findByParameter("temp_token");
				if (sendXampiToken().hasBody()) {
					JSONObject json = new JSONObject(sendXampiToken().getBody());
					token = json.optString("access_token");
					tempToken.setValue(token);
					tempToken.setRegisterDate(addMinutes(15));
					cfg.save(tempToken);
					// intenta otra vez
					return sendXampiTry2s(encriptBody);
				}
			}
			
			try {
				res = ecriptUtil.decryptJose(k1.getResponseBodyAsString(), key);
				response = new JSONObject(res);
				JSONObject respo = response.getJSONObject("response");
				JSONObject curpValidation = new JSONObject();
				curpValidation.put("fatherLastNameMatch", respo.optBoolean("fatherLastName"));
				curpValidation.put("curpMatch", respo.optBoolean("curp"));
				curpValidation.put("motherLastNameMatch", respo.optBoolean("motherLastName"));
				curpValidation.put("nameMatch", respo.optBoolean("name"));
				respo.put("curpValidation", curpValidation);
				response.put("response", respo);			

			} catch (JoseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new ResponseEntity<>(response.toString(), k1.getStatusCode());
		} catch (RestClientException e) {
			if(e.getMessage().contains("500")) {
				return sendXampiTry2s(encriptBody);
			}
			log.error("ERROR: error en el llamado al servicio Xapi");
			return new ResponseEntity<>("ERROR: RestClientException" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (JoseException e) {
			log.error("Error: Error en la encriptacion. " + e.getMessage());
			return new ResponseEntity<>("ERROR: Error en la encriptacion. " + e.getMessage(), 	HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private ResponseEntity<String> sendXampiTry2s(String encriptBody) {		
		log.info("INFO:  llamando  a validacion ine ---->> " + uuid);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("AUTHORIZATION", token);
		headers.set("HEADER.APPLICATIONID", applicationid);// MOBAPP
		headers.set("HEADER.TRACKING_ID", tracking_id+"_"+uuid);// hj38ds8bhgrx8z6
		headers.set("HEADER.SERVICE_CHANNEL", service_channel);// IB3
		headers.set("HEADER.SERVICE_PROCEDENCE", service_procedence);// internet
		headers.set("HEADER.SERVICE_LANGUAGE", service_language); // ES
		headers.set("HEADER.SERVICE_COUNTRY", service_country);// MX
		headers.set("x-ibm-client-id", clientIdP);
		headers.set("x-ibm-client-secret", clientSecretP);
		headers.set("X-Client-Certificate", clientCertificate);
		JSONObject response = null;
		try {
			// Encrypt
			HttpEntity<?> entity = new HttpEntity<>(encriptBody, headers);
//			HttpEntity<?> entity = new HttpEntity<>(body, headers);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> repo = restTemplate.exchange(xapiPath, HttpMethod.POST, entity, String.class);

			log.info("INFO: HEADERS:" + new JSONObject(repo.getHeaders()).toString());

			log.info("INFO: respuesta validacion ine ---->> " + repo.getBody());
			EncryptAesUtil ecriptUtil = new EncryptAesUtil();
			String res = ecriptUtil.decryptJose(repo.getBody(), key);

			response = new JSONObject(res);
			JSONObject respo = response.getJSONObject("response");
			JSONObject curpValidation = new JSONObject();
			curpValidation.put("fatherLastNameMatch", respo.optBoolean("fatherLastName"));
			curpValidation.put("curpMatch", respo.optBoolean("curp"));
			curpValidation.put("motherLastNameMatch", respo.optBoolean("motherLastName"));
			curpValidation.put("nameMatch", respo.optBoolean("name"));
			respo.put("curpValidation", curpValidation);
			response.put("response", respo);
			log.info("INFO: respuesta validacion ine ---->> " + response.toString());
			return new ResponseEntity<>(response.toString(), HttpStatus.OK);
		} catch (HttpClientErrorException k1) {
			log.error("Http code is not 2XX. The server responded: " + k1.getStatusCode() + " Cause: "
					+ k1.getResponseBodyAsString());
			EncryptAesUtil ecriptUtil = new EncryptAesUtil();
			String res = "";				
			try {
				res = ecriptUtil.decryptJose(k1.getResponseBodyAsString(), key);
				response = new JSONObject(res);
				JSONObject respo = response.getJSONObject("response");
				JSONObject curpValidation = new JSONObject();
				curpValidation.put("fatherLastNameMatch", respo.optBoolean("fatherLastName"));
				curpValidation.put("curpMatch", respo.optBoolean("curp"));
				curpValidation.put("motherLastNameMatch", respo.optBoolean("motherLastName"));
				curpValidation.put("nameMatch", respo.optBoolean("name"));
				respo.put("curpValidation", curpValidation);
				response.put("response", respo);			

			} catch (JoseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new ResponseEntity<>(response.toString(), k1.getStatusCode());
		} catch (RestClientException e) {
			log.error("ERROR: error en el llamado al servicio Xapi");
			return new ResponseEntity<>("ERROR: RestClientException" + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (JoseException e) {
			log.error("Error: Error en la encriptacion. " + e.getMessage());
			return new ResponseEntity<>("ERROR: Error en la encriptacion. " + e.getMessage(), 	HttpStatus.INTERNAL_SERVER_ERROR);
		}
	} 

	public ResponseEntity<String> sendXampiToken() {
		log.info("INFO:  actualizando token ---->> ");
		HttpHeaders headers = new HttpHeaders();
		headers.set("content-type", MediaType.APPLICATION_JSON_VALUE);
		headers.set("header.applicationid", tokenAplicationId);
		headers.set("header.service_channel", tokenServiceChannel);
		headers.set("header.service_country", serviceCountry);
		headers.set("header.service_language", serviceLanguage);
		headers.set("header.service_procedence", serviceProcedence);
		headers.set("header.tracking_id", tracking_id+"_"+uuid);
		headers.set("x-ibm-client-id", clientId);
		headers.set("x-ibm-client-secret", clientSecret);
		headers.set("X-Client-Certificate", clientCertificate);
		headers.set("header.rsa_data", rsaData);
		JSONObject body = new JSONObject();
		body.put("password", tokenPassword);
		body.put("user", tokenUser);
		EncryptAesUtil ecriptUtil = new EncryptAesUtil();
		String reqEncript = "";
		try {
			reqEncript = ecriptUtil.encryptJose(body.toString(), key);
		} catch (JoseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			HttpEntity<?> entity = new HttpEntity<>(reqEncript, headers);
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> resp = restTemplate.exchange(tokenPath, HttpMethod.POST, entity, String.class);
			log.info("resp > " + resp);
			String respp = null;
			if (resp.hasBody()) {
				try {
					respp = ecriptUtil.decryptJose(resp.getBody(), key);
				} catch (JoseException e) {
					e.printStackTrace();
				}
			}
			log.info("INFO: respuesta sendXampiToken---->> " + respp);
			return new ResponseEntity<>(respp, HttpStatus.OK);

		} catch (RestClientException e) {
			log.error("ERROR: error en el llamado al servicio sendXampiToken:" + e.getMessage());
			return new ResponseEntity<>("ERROR: RestClientException" + e.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
		}
	}

	public ResponseEntity<String> ineValidate(ValidateIneRequest validateIne, String request) {
	
		log.info("TEST CERO");
		if (validateIne.getName().trim().equals("TEST CERO")) {
			return responseMokup(validateIne);
		}
		log.info("TEST1");
		if (validateIne.getName().trim().equals("TEST1")) {
			return responseMokupFaill(validateIne);
		}
		log.info("TEST2");
		if (validateIne.getName().trim().equals("TEST2")) {
			return responseMokupFaill2(validateIne);
		}
		log.info("TEST3");
		if (validateIne.getName().trim().equals("TEST3")) {
			return responseMokupFaill3(validateIne);
		}
		// after test--->
		log.info("init");
		init();
//		 "location":DEFAULT
		Location loc = new Location();
		loc.setCity("CDMX");
		loc.setState("9");
		loc.setZipCode("11520");
		loc.setLatitude(19.4413655F);
		loc.setLongitude(-99.1989333F);
		Others others = new Others();
		others.setLocationObject(loc);
		validateIne.getOthersObject().setLocationObject(loc);
		JSONObject json = new JSONObject(request);
		EncryptAesUtil ecriptUtil = new EncryptAesUtil();
		String encriptBody = null;
		try {
			JSONObject other = json.getJSONObject("others");
			JSONObject location = other.getJSONObject("location");
			location.put("zipCode", "11520");
			location.put("city", "CDMX");
			location.put("latitude", "19.4413655F");
			location.put("state", "9");
			location.put("longitude", "-99.1989333F");

			// validar dedos vacios
			JSONArray biometrics = other.optJSONArray("biometrics");
//			log.info("#######>>>>>"+biometrics.getJSONObject(0).optString("fingerPrint").length());
//			log.info("#######>>>>>"+biometrics.getJSONObject(1).optString("fingerPrint").length());
			
			
			log.info("for");
			for (int i=0; i<biometrics.length(); i++) {
				log.info(biometrics.getJSONObject(i).optString("fingerNumber"));
//			    JSONObject item = biometrics.getJSONObject(i);
				if (biometrics.getJSONObject(i).optString("fingerPrint").isEmpty() || 
						biometrics.getJSONObject(i).optString("fingerPrint").length() < 5 ) {
//				if (biometrics.getJSONObject(i).optString("fingerPrint").length() < 5 ) {
					log.info("fingerNumber");
					log.info(biometrics.getJSONObject(i).optString("fingerNumber"));
					JSONObject jo = biometrics.getJSONObject(i);
					jo.put("fingerPrint", "");
					biometrics.put(i, jo);
				}
			}
			
			
			
//			if (biometrics != null && biometrics.getJSONObject(0).optString("fingerPrint").length() < 5
//					|| biometrics.getJSONObject(1).optString("fingerPrint").length() < 5) {
//
//				if (biometrics.getJSONObject(0).optString("fingerPrint").length() < 5 ) {
//					JSONObject jo = biometrics.getJSONObject(0);
//					jo.put("fingerPrint", "");
//					biometrics.put(0, jo);
//				}
//				if (biometrics.getJSONObject(1).optString("fingerPrint").length() < 5 ) {
//					JSONObject jo1 = biometrics.getJSONObject(1);
//					jo1.put("fingerPrint", "");
//					biometrics.put(1, jo1);
//				}
//			}
//			
//			
//			log.info("biometrics");
//			
//			if (biometrics.getJSONObject(2) != null) {
//				if (biometrics.getJSONObject(2).optString("fingerPrint").length() < 5 ) {
//					JSONObject b = new JSONObject();
//					b = biometrics.optJSONObject(2);
//					b.put("fingerPrint", "");
//					biometrics.put(2, b);
//				}
//			}
//			if (biometrics.getJSONObject(3) != null) {
//				if (biometrics.getJSONObject(3).optString("fingerPrint").length() < 5 ) {
//					JSONObject b = new JSONObject();
//					b = biometrics.optJSONObject(3);
//					b.put("fingerPrint", "");
//					biometrics.put(3, b);
//				}
//			}
//			if (biometrics.getJSONObject(4) != null) {
//				if (biometrics.getJSONObject(4).optString("fingerPrint").length() < 5 ) {
//					JSONObject b = new JSONObject();
//					b = biometrics.optJSONObject(4);
//					b.put("fingerPrint", "");
//					biometrics.put(4, b);
//				}
//			}
//			if (biometrics.getJSONObject(5) != null) {
//				if (biometrics.getJSONObject(5).optString("fingerPrint").length() < 5 ) {
//					JSONObject b = new JSONObject();
//					b = biometrics.optJSONObject(5);
//					b.put("fingerPrint", "");
//					biometrics.put(5, b);
//				}
//			}
//			if (biometrics.getJSONObject(6) != null) {
//				if (biometrics.getJSONObject(6).optString("fingerPrint").length() < 5 ) {
//					JSONObject b = new JSONObject();
//					b = biometrics.optJSONObject(6);
//					b.put("fingerPrint", "");
//					biometrics.put(6, b);
//				}
//			}
//			if (biometrics.getJSONObject(7) != null) {
//				if (biometrics.getJSONObject(7).optString("fingerPrint").length() < 5 ) {
//					JSONObject b = new JSONObject();
//					b = biometrics.optJSONObject(7);
//					b.put("fingerPrint", "");
//					biometrics.put(7, b);
//				}
//			}
//			if (biometrics.getJSONObject(8) != null) {
//				if (biometrics.getJSONObject(8).optString("fingerPrint").length() < 5 ) {
//					JSONObject b = new JSONObject();
//					b = biometrics.optJSONObject(8);
//					b.put("fingerPrint", "");
//					biometrics.put(8, b);
//				}
//			}
			
			log.info("biometrics 3");
			
			other.put("biometrics", biometrics);

			json.put("others", other);
			
			String cic = json.optString("cic");
			if(cic.length()<=8) {
				json.put("cic", JSONObject.NULL);
			}
			String fatherLastName = json.optString("fatherLastName");
			if(fatherLastName.length()<=2) {
				json.put("fatherLastName", JSONObject.NULL);
			}
			String motherLastName = json.optString("motherLastName");
			if(motherLastName.length()<=2) {
				json.put("motherLastName", JSONObject.NULL);
			}
//			log.info("INPUT:---->    " + json);
			encriptBody = ecriptUtil.encryptJose(json.toString(), key);
		} catch (JoseException e) {
			e.printStackTrace();
		}
		
		return sendXampi(encriptBody);
	}

	private ResponseEntity<String> responseMokup(ValidateIneRequest validateIne) {
		String response = "{\"response\":{\"curpValidation\":{\"nameMatch\":true,\"fatherLastNameMatch\":true,\"motherLastNameMatch\":true,\"curpMatch\":true},\"ocr\":true,\"name\":true,\"fatherLastName\":true,\"motherLastName\":true,\"registryYear\":true,\"emissionYear\":true,\"electorCode\":true,\"emissionNumber\":true,\"curp\":true,\"registrationSituation\":\"VIGENTE\",\"stoleLostReport\":\"VIGENTE\",\"additional\":{\"biometrics\":{\"matchFingerRigth\":\"98.0%\",\"matchFingerLeft\":\"98.0%\"},\"idRequest\":\"ce335c2cdaea4ded8e\"}}}";
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	private ResponseEntity<String> responseMokupFaill(ValidateIneRequest validateIne) {
		String response = "{\"response\":{\"curpValidation\":{\"nameMatch\":true,\"fatherLastNameMatch\":true,\"motherLastNameMatch\":true,\"curpMatch\":true},\"ocr\":false,\"name\":true,\"fatherLastName\":true,\"motherLastName\":true,\"registryYear\":true,\"emissionYear\":true,\"electorCode\":true,\"emissionNumber\":true,\"curp\":true,\"registrationSituation\":\"VIGENTE\",\"stoleLostReport\":\"VIGENTE\",\"additional\":{\"biometrics\":{\"matchFingerRigth\":\"98.0%\",\"matchFingerLeft\":\"99.0%\"},\"idRequest\":\"ce335c2cdaea4ded8e\"}}}";
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	private ResponseEntity<String> responseMokupFaill2(ValidateIneRequest validateIne) {
		String response = "{\"response\":{\"curpValidation\":{\"nameMatch\":true,\"fatherLastNameMatch\":true,\"motherLastNameMatch\":true,\"curpMatch\":true},\"ocr\":true,\"name\":true,\"fatherLastName\":true,\"motherLastName\":true,\"registryYear\":true,\"emissionYear\":true,\"electorCode\":true,\"emissionNumber\":true,\"curp\":true,\"registrationSituation\":\"VIGENTE\",\"stoleLostReport\":\"VIGENTE\",\"additional\":{\"biometrics\":{\"matchFingerRigth\":\"98.0%\",\"matchFingerLeft\":\"0.0%\"},\"idRequest\":\"ce335c2cdaea4ded8e\"}}}";
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	private ResponseEntity<String> responseMokupFaill3(ValidateIneRequest validateIne) {
		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}