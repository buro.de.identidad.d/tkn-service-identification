package com.teknei.bid.service.ine.request;

import lombok.Data;

@Data
public class Location {
	private float latitude;
	private float longitude;
//	private String country;
	private String city;
	private String state;
	private String zipCode;

}
