package com.teknei.bid.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FimpeResponse {

	private String codigoRespuesta;
	private String descripcionRespuesta;
	private Boolean respuestaServicio;
	
}
