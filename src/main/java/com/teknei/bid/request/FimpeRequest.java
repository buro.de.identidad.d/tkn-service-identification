package com.teknei.bid.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FimpeRequest {

	private String versionJSON;
	private DatosCliente datosCliente;
	private DatosInstitucion datosInstitucion;
	private String minucia2;
	private String minucia7;
	private DatosMAFI datosMAFI;
	
	@Data
	@NoArgsConstructor
	public class DatosCliente {

		private String nombre;
		private String apellidoPaterno;
		private String apellidoMaterno;
		private String ocr;
		private String anioRegistro;
		private String anioEmision;
		private String consentimiento;
		private String numeroEmision;
		private String claveElector;
		private String cic;
		private String curp;
	}
	
	@Data
	@NoArgsConstructor
	public class DatosInstitucion {

		private String idInstitucion;
		private String idEstacion;
		private String fechaHora;
		private String referencia;
		private Double latitud;
		private Double longitud;
		private String codigoPostal;
		private String ciudad;
		private String estado;
	}
	
	@Data
	@NoArgsConstructor
	public class DatosMAFI {

		private String nombreMAFI;
		private String versionMAFI;
		private String tipoLector;
		private Integer idParametros;
		private Integer tiempoLectura;
		private Integer intentosLectura;
	}
}
