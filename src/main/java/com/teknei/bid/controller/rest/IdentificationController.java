package com.teknei.bid.controller.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Phrase;
import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.BasicRequestDTO;
import com.teknei.bid.dto.CurpRequestDTO;
import com.teknei.bid.dto.DocumentPictureRequestDTO;
import com.teknei.bid.dto.IneDetailDTO;
import com.teknei.bid.dto.PersonDataIneTKNRequest;
import com.teknei.bid.dto.RequestEncFilesDTO;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieIfeIne;
import com.teknei.bid.persistence.entities.BidClieIfeInePK;
import com.teknei.bid.persistence.entities.BidClieMail;
import com.teknei.bid.persistence.entities.BidClieRegConsultIne;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.repository.BidClieRegConsultIneRepository;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidIfeRepository;
import com.teknei.bid.request.FimpeRequest;
import com.teknei.bid.response.FimpeResponse;
import com.teknei.bid.service.CurpService;
import com.teknei.bid.service.Fimpe;
import com.teknei.bid.service.IdentificationService;
import com.teknei.bid.service.IneResultDocumentService;
import com.teknei.bid.service.LogUtil;
import com.teknei.bid.service.ine.IneValidateService;
import com.teknei.bid.service.ine.XapiService;
import com.teknei.bid.service.ine.request.Biometric;
import com.teknei.bid.service.ine.request.Others;
import com.teknei.bid.service.ine.request.ValidateIneRequest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "/identification")
public class IdentificationController {

	@Autowired
	@Qualifier(value = "storeTasAdditionalCredentialCommand")
	private Command storeTasAdditionalCredentialCommand;
	@Autowired
	@Qualifier(value = "credentialCommand")
	private Command credentialCommand;
	@Autowired
	@Qualifier(value = "additionalCredentialCommand")
	private Command additionalCredentialsCommand;
	@Autowired
	@Qualifier(value = "captureCommand")
	private Command captureCommand;
	@Autowired
	private IdentificationService identificationService;
	@Autowired
	private BidIfeRepository bidIfeRepository;
	@Autowired
	private BidCurpRepository bidCurpRepository;
	@Autowired
	private BidEstaProcRepository bidEstaProcRepository;
	@Autowired
	private BidClieRegEstaRepository bidClieRegEstaRepository;
	@Autowired
	private CurpService curpService;
	@Autowired
	private XapiService xapiService;
	@Autowired
	private IneValidateService ineValidateService;
	@Autowired
	private BidClieRegConsultIneRepository bidClieRegConsultIneRepository;
	@Autowired
	private Fimpe fimpe;
	@Autowired
	private IneResultDocumentService ineResultDocumentService;
	@Autowired
	private BidClieRepository bidClieRep;

	private static final String ESTA_PROC = "VAL-SERV-INE";
	private static final String ESTA_PROC_2 = "RES-INE";
	private static final Logger log = LoggerFactory.getLogger(IdentificationController.class);

	@ApiOperation(value = "Verifies the information against INE CECOBAN stage", response = String.class)
	@RequestMapping(value = "/verify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> verifyAgainstINE(@RequestBody PersonDataIneTKNRequest personData) {
		try {
			updateStatus(personData.getId(), ESTA_PROC);
			JSONObject jsonObject = identificationService.verifyCecoban(personData);
			// TODO save request? save response from remote servera
			updateStatus(personData.getId(), ESTA_PROC_2);
			return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error for make request to cecoban with message: {}", e.getMessage());
			return new ResponseEntity<>("Error", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@ApiOperation(value = "Finds the detail parsed for a particular parsed address", response = IneDetailDTO.class)
	@RequestMapping(value = "/findDetail/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<IneDetailDTO> findDetail(@PathVariable Long id) {
		IneDetailDTO dto = new IneDetailDTO();
		try {
			BidClieIfeInePK pk = new BidClieIfeInePK();
			pk.setIdClie(id);
			pk.setIdIfe(id);
			BidClieIfeIne retrieved = bidIfeRepository.findOne(pk);
			if (retrieved == null) {
				return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
			}
			dto.setApeMat(retrieved.getApeMate());
			dto.setApePat(retrieved.getApePate());
			dto.setCall(retrieved.getCall());
			dto.setClavElec(retrieved.getClavElec());
			dto.setCol(retrieved.getCol());
			dto.setCp(retrieved.getCp());
			dto.setDist(retrieved.getDist());
			dto.setEsta(retrieved.getEsta());
			dto.setFoli(retrieved.getFoli());
			dto.setLoca(retrieved.getLoca());
			dto.setMrz(retrieved.getMrz());
			dto.setMuni(retrieved.getMuni());
			dto.setNoExt(retrieved.getNoExt());
			dto.setNoInt(retrieved.getNoInt());
			dto.setOcr(retrieved.getOcr());
			dto.setNomb(retrieved.getNomb());
			dto.setSecc(retrieved.getSecc());
			if (retrieved.getVige() != null) {
				dto.setVige(retrieved.getVige().getTime());
			}
			return new ResponseEntity<>(dto, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error finding detail for: {} with message: {}", id, e.getMessage());
			return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@ApiOperation(value = "Updates manually data retrieved by parsing systems", notes = "The 'user' field represents the user logged into system that is actually making the update", response = String.class)
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> updateCredentialData(@PathVariable Long id, @RequestBody IneDetailDTO detailDTO) {
		if (detailDTO.getCurp() != null && !detailDTO.getCurp().isEmpty()) {
			try {
				bidCurpRepository.updateCurp(detailDTO.getCurp(), id);
			} catch (Exception e) {
				log.error("Error updating base curp for proces with message: {}", e.getMessage());
			}
		}
		try {
			BidClieIfeInePK pk = new BidClieIfeInePK();
			pk.setIdClie(id);
			pk.setIdIfe(id);
			BidClieIfeIne retrieved = bidIfeRepository.findOne(pk);
			retrieved.setNomb(detailDTO.getNomb());
			retrieved.setApePate(detailDTO.getApePat());
			retrieved.setApeMate(detailDTO.getApeMat());
			retrieved.setCall(detailDTO.getCall());
			retrieved.setNoExt(detailDTO.getNoExt());
			retrieved.setNoInt(detailDTO.getNoInt());
			retrieved.setCol(detailDTO.getCol());
			retrieved.setCp(detailDTO.getCp());
			retrieved.setFoli(detailDTO.getFoli());
			retrieved.setClavElec(detailDTO.getClavElec());
			retrieved.setOcr(detailDTO.getOcr());
			retrieved.setEsta(detailDTO.getEsta());
			retrieved.setMuni(detailDTO.getMuni());
			retrieved.setLoca(detailDTO.getLoca());
			retrieved.setDist(detailDTO.getDist());
			retrieved.setSecc(detailDTO.getSecc());
			retrieved.setVige(new Timestamp(detailDTO.getVige()));
			retrieved.setMrz(detailDTO.getMrz());
			retrieved.setFchModi(new Timestamp(System.currentTimeMillis()));
			retrieved.setUsrModi(detailDTO.getUser());
			retrieved.setCurp(detailDTO.getCurp());
			retrieved.setUsrOpeModi(detailDTO.getUser());
			bidIfeRepository.save(retrieved);
			return new ResponseEntity<>("Successful update", HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error updating manually address {} for id : {} with message: {}", id, detailDTO, e.getMessage());
			return new ResponseEntity<>("Error updating detail", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@ApiOperation(value = "Uploads file(s) related to the additional identification of the customer, parses it and stores in the casefile", notes = "The attachment(s) must be named 'file'. The first identification must be front, the second (optional) must be back", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "If the process is correct"),
			@ApiResponse(code = 422, message = "If the identification could not be recognized") })
	@RequestMapping(value = "/uploadAdditional/{id}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> uploadAdditionalCredentials(@RequestPart(value = "file") List<MultipartFile> files,
			@PathVariable Long id) {
		return upload(files, id, additionalCredentialsCommand);
	}

	@ApiOperation(value = "Uploads file(s) related to the additional identification in plain of the customer, parses it and stores in the casefile", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "If the process is correct"),
			@ApiResponse(code = 422, message = "If the identification could not be recognized") })
	@RequestMapping(value = "/uploadAdditionalPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> uploadAdditionalCredentials(@RequestBody RequestEncFilesDTO dto) {
		return processEncFiles(dto, additionalCredentialsCommand);
	}

	@ApiOperation(value = "Uploads capture screen related to the additional identification of the customer, parses it and stores in the casefile", notes = "The attachment(s) must be named 'file", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "If the process is correct"),
			@ApiResponse(code = 422, message = "If the capture could not be recognized") })
	@RequestMapping(value = "/uploadCapture/{id}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> uploadCaptureCredentials(@RequestPart(value = "file") List<MultipartFile> files,
			@PathVariable Long id) {
		return upload(files, id, captureCommand);
	}

	@ApiOperation(value = "Uploads capture screen in plain related to the additional identification of the customer, parses it and stores in the casefile", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "If the process is correct"),
			@ApiResponse(code = 422, message = "If the capture could not be recognized") })
	@RequestMapping(value = "/uploadCapturePlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> uploadCaptureCredentials(@RequestBody RequestEncFilesDTO dto) {
		return processEncFiles(dto, captureCommand);
	}

	@ApiOperation(value = "Uploads file(s) related to the identification of the customer, parses it and stores in the casefile", notes = "The attachment(s) must be named 'file'", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "If the process is correct"),
			@ApiResponse(code = 422, message = "If the identification could not be recognized") })
	@RequestMapping(value = "/upload/{id}", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<String> uploadCredentials(@RequestPart(value = "file") List<MultipartFile> files,
			@PathVariable Long id) {
		return upload(files, id, credentialCommand);
	}

	@ApiOperation(value = "Uploads file(s) related to the identification of the customer, parses it and stores in the casefile", response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "If the process is correct"),
			@ApiResponse(code = 422, message = "If the identification could not be recognized") })
	@RequestMapping(value = "/uploadPlain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> uploadCredentialsAsync(@RequestBody RequestEncFilesDTO dto) {
		return processEncFiles(dto, credentialCommand);
	}

	@ApiOperation(value = "Downloads the resource (identification) related to the customer", response = byte[].class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "If the picture is found"),
			@ApiResponse(code = 404, message = "If the picture is nof found"),
			@ApiResponse(code = 500, message = "Internal server error") })
	@RequestMapping(value = "/download", method = RequestMethod.POST)
	public ResponseEntity<byte[]> getImageFromReference(@RequestBody DocumentPictureRequestDTO dto) {
		byte[] image = null;
		try {
			image = identificationService.findPicture(dto);
			if (image == null) {
				return new ResponseEntity<>(image, HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(image, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Error finding image for reference: {} with message: {}", dto, e.getMessage());
			return new ResponseEntity<>(image, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/curp/obtain", method = RequestMethod.POST)
	public ResponseEntity<String> getCurp(@RequestBody CurpRequestDTO curpRequestDTO) {
		String response = null;
		switch (curpRequestDTO.getType()) {
		case 1:
			response = curpService.getCurp(curpRequestDTO.getRequestByCustomerData().getIdCustomer(),
					curpRequestDTO.getRequestByCustomerData().getEntidad(), curpRequestDTO.getDocument());
			break;
		case 2:
			response = curpService.getCurp(curpRequestDTO.getRequestByExternalData().getName(),
					curpRequestDTO.getRequestByExternalData().getSurname(),
					curpRequestDTO.getRequestByExternalData().getSurnameLast(),
					curpRequestDTO.getRequestByExternalData().getBirthDate(),
					curpRequestDTO.getRequestByExternalData().getEntidad(),
					curpRequestDTO.getRequestByExternalData().getGender(), curpRequestDTO.getDocument());
			break;
		default:
			response = null;
			break;
		}
		if (response == null) {
			return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/curp/validate", method = RequestMethod.POST)
	public ResponseEntity<String> validateCurp(@RequestBody CurpRequestDTO curpRequestDTO) {
		String response = null;
		switch (curpRequestDTO.getType()) {
		case 3:
			response = curpService.validateCurp(curpRequestDTO.getValidateByCustomerData().getIdCustomer(),
					curpRequestDTO.getDocument());
			break;
		case 4:
			response = curpService.validateCurp(curpRequestDTO.getValidateByExternalData().getCurp(),
					curpRequestDTO.getDocument());
			break;
		default:
			response = null;
			break;
		}
		if (response == null) {
			return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@ApiOperation(value = "Rollback the status for primary id captured")
	@RequestMapping(value = "/rollback/identificationCapture", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> rollbackConfirmationCredentialCapture(@RequestBody BasicRequestDTO basicRequestDTO) {
		List<String> listToRollback = Arrays.asList(new String[] { "CAP-CRE", "CAP-VALINE" });
		listToRollback.forEach(r -> rollbackStatus(basicRequestDTO.getIdOperation(), r, basicRequestDTO.getUsername()));
		return new ResponseEntity<>("OK", HttpStatus.OK);
	}

	private ResponseEntity<String> upload(List<MultipartFile> files, Long id, Command processingCommand) {
		CommandRequest request = new CommandRequest();
		request.setFiles(files);
		request.setId(id);
		request.setUsername("tkn-api");
		CommandResponse response = processingCommand.execute(request);
		if (response.getStatus().equals(Status.CREDENTIALS_OK)) {
			return new ResponseEntity<>(response.getDesc(), HttpStatus.OK);
		}
		return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
	}

	private void updateStatus(Long idClient, String status) {
		try {
			log.info("Update Status ---------------------------------  >>> ");
			BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
			BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient,
					estaProc.getIdEstaProc());
			if (regEsta == null) {
				log.warn("Status for process: {} found null", idClient);
			}
			regEsta.setEstaConf(true);
			regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
			regEsta.setUsrModi("client-api");
			BidClieRegEsta res = bidClieRegEstaRepository.save(regEsta);

			log.info("Update Status ---------------------------------  >>> " + res.getIdClie());
		} catch (Exception e) {
			log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
		}
	}

	private void rollbackStatus(Long idClient, String status, String username) {
		try {
			BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(status, 1);
			BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient,
					estaProc.getIdEstaProc());
			if (regEsta == null) {
				log.warn("Status for process: {} found null", idClient);
			}
			regEsta.setEstaConf(false);
			regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
			regEsta.setUsrModi(username);
			regEsta.setUsrOpeModi(username);
			bidClieRegEstaRepository.save(regEsta);
		} catch (Exception e) {
			log.error("Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
		}
	}

	private ResponseEntity<String> processEncFiles(RequestEncFilesDTO dto, Command processingCommand) {
		CommandRequest request = new CommandRequest();
		request.setUsername(dto.getUsername());
		ArrayList<byte[]> contentFiles = new ArrayList<>();
		byte[] content1 = Base64Utils.decodeFromString(dto.getB64Anverse());
		contentFiles.add(content1);
		byte[] content2 = null;
		if (dto.getB64Reverse() != null && !dto.getB64Reverse().isEmpty()) {
			content2 = Base64Utils.decodeFromString(dto.getB64Reverse());
			contentFiles.add(content2);
		}
		request.setFileContent(contentFiles);
		request.setId(dto.getOperationId());
		CommandResponse response = processingCommand.execute(request);
		if (response.getStatus().equals(Status.CREDENTIALS_OK)) {
			return new ResponseEntity<>(response.getDesc(), HttpStatus.OK);
		}
		return new ResponseEntity<>(String.valueOf(response.getStatus().getValue()), HttpStatus.UNPROCESSABLE_ENTITY);
	}

	// --------------------------------------------------------------------------------------------------------------

	@RequestMapping(value = "/validation", method = RequestMethod.POST)
	public ResponseEntity<FimpeResponse> valid(@RequestBody FimpeRequest fimpeRequest) {
		log.info("INE Validation Begin----->>>");
		FimpeResponse result = fimpe.ineVerification(fimpeRequest);

		if (result != null) {
			return new ResponseEntity<>(result, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@Transactional
	@RequestMapping(value = "/xapi/validation", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> ineValidate(@RequestBody String request) {
		log.info("INE ValidateIneRequest Begin----->>>");
		// ----------------------------DUMMY-------------------------------
		JSONObject dataRequ = new JSONObject(request);
		boolean validateIne = dataRequ.optBoolean("isValidateIne");
		Long idClie = dataRequ.optLong("idOperation");
		log.info("INFO > "+idClie+" ES TIPO DE OPERACION INE ? "+validateIne);
//		boolean isDummyOK = dataRequ.optBoolean("isDummyOK");
//		Long idOperation = dataRequ.optLong("idOperation");
//		log.info("is dummy >"+(isDummyOK && validateIne));
//		if (isDummyOK && validateIne) {
////			updateStatus(idOperation, "FIR-CONT");
//			updateStatus(idOperation, "FIR-CONT");
//			JSONObject dataRequest = new JSONObject();
//			dataRequest.put("errorCode", "00");
//			return new ResponseEntity<>(dataRequest.toString(), HttpStatus.OK);
//		}
		// ----------------------------------------------------------
		log.info("ValidateIneRequest");
		ValidateIneRequest validateIneRequest = validateIneMapper(request);
		BidClieRegConsultIne regIntentos = new BidClieRegConsultIne();
		List<BidClieRegConsultIne> intentos = bidClieRegConsultIneRepository
				.findByidClie(validateIneRequest.getIdOperation());
		Boolean intento3 = false;
		int numIntentoPrev = 0;
		if (intentos != null && !intentos.isEmpty()) {
			BidClieRegConsultIne intento = intentos.get(0);
			intento3 = intento.getIntento() >= 2;
			if (intento.getIntento() >= 3) { // validacion 3 intentos 
				JSONObject respuesta = new JSONObject();
				respuesta.put("request", new JSONObject(intento.getDataRequest()));
				respuesta.put("errorCode", "04");
				respuesta.put("errorMessage", "Limite de intentos alcanzado.");
				respuesta.put("regIntentos", new JSONObject(regIntentos));
				return new ResponseEntity<>(respuesta.toString(), HttpStatus.OK);
			}
			numIntentoPrev = intento.getIntento();
			// se encontro un intento previo
			regIntentos = intentos.get(0);
			regIntentos.setIntento(numIntentoPrev + 1);
			regIntentos.setLastUpdateDate(new Timestamp(new Date().getTime()));
		} else {
			// no se encontro un intento previo
			regIntentos.setIdClie(validateIneRequest.getIdOperation());
			regIntentos.setIntento(1);
			regIntentos.setRegisterDate(new Timestamp(new Date().getTime()));
		}

		// realizando la peticion a validar
		log.info("realizando la peticion a validar");
		JSONObject dataRequest = new JSONObject();
		ResponseEntity<String> res = xapiService.ineValidate(validateIneRequest, request);
		if (res.getStatusCode().equals(HttpStatus.OK)) {
			// realizando la validacion de respuesta correcta
			log.info("realizando la validacion de respuesta correcta");
			List<String> errorCodeList = ineValidateService.validateResponseIne(res.getBody());
			String errorCode;
			if (errorCodeList.isEmpty()) {
				errorCode = "00";
			} else {
				errorCode = errorCodeList.get(0);
			}
			if (intento3 && !errorCode.equals("00")) { // validacion 3 intentos
				JSONObject respuesta = new JSONObject();
				respuesta.put("request", new JSONObject(res.getBody()));
				respuesta.put("errorCode", "04");
				respuesta.put("errorMessage", "Limite de intentos alcanzado.");
				respuesta.put("regIntentos", new JSONObject(regIntentos));
				return new ResponseEntity<>(respuesta.toString(), HttpStatus.OK);
			}
			regIntentos.setRegbiom(true);
			regIntentos.setReginfo(true);
			for (String error : errorCodeList) {
				// fallo biometria
				if (error.equals("07")) {
					regIntentos.setRegbiom(false);
				}

				// fallo informacion
				if (error.equals("01") || error.equals("05") || error.equals("08")) {
					regIntentos.setReginfo(false);
				}

			}
			log.info("setReginfo-->" + regIntentos.getReginfo());
			log.info("getRegbiom-->" + regIntentos.getRegbiom());
			dataRequest.put("request", new JSONObject(request));
			dataRequest.put("response", new JSONObject(res.getBody()));
			dataRequest.put("errorCode", errorCode);
			regIntentos.setDataRequest(dataRequest.toString());
			regIntentos.setSent(true);// se ejecuto el llamado
			regIntentos = bidClieRegConsultIneRepository.save(regIntentos);
			
			// TODO ACTUALIZAR LA OPERACION
			//TODO NO HAY OPERACION VALIDAR DEPENDENCIAS CON OPERACION Y EVADIRLAS con bandera isValidateIne

						if (dataRequest.optString("errorCode").equals("00") && validateIne) {
							
							updateStatus(idClie, "FIR-CONT");
							// TODO actualizar datos
							BidClie bidClie = bidClieRep.getOne(idClie);				
							bidClie.setApePate(dataRequ.optString("fatherLastName"));
							bidClie.setApeMate(dataRequ.optString("motherLastName"));
							bidClie.setFchModi(new Timestamp(System.currentTimeMillis()));
							bidClie.setNomClie(dataRequ.optString("name"));
							bidClie.setUsrModi("client-ine");
							bidClieRep.save(bidClie);				
							// 2nd step - save the curp
//							BidClieCurp bidClieCurp = bidCurpRepository.findTopByIdClie(idClie);
//							bidClieCurp.setCurp(dataRequ.getString("curp")+"_"+idClie+"_INE");
//							bidClieCurp.setFchModi(new Timestamp(System.currentTimeMillis()));
//							bidClieCurp.setUsrModi("client-ine");
//							bidCurpRepository.save(bidClieCurp);

						}

			
//			if(dataRequest.optString("errorCode").equals("00")) {
			JSONObject response = dataRequest.getJSONObject("response");
			response.put("registerDate", regIntentos.getRegisterDate());
			response.put("intento", regIntentos.getIntento());
			response.put("errorCode", dataRequest.optString("errorCode"));
			log.info("INFO: **************** Generndo rporte de consulta al INE en TAS ******************* ");
			ineResultDocumentService.getInVerification(dataRequest.getJSONObject("request"), response,
					regIntentos.getIntento());
	
			log.info("INE ValidateIneRequest End----->>>");
			JSONObject respuesta = new JSONObject(res.getBody());
			respuesta.put("errorCode", errorCode);
			JSONObject regIntentosJson = new JSONObject();
			regIntentosJson.put("id_clie", regIntentos.getIdClie());
			regIntentosJson.put("regbiom", regIntentos.getRegbiom());
			regIntentosJson.put("reginfo", regIntentos.getReginfo());
			regIntentosJson.put("intento", regIntentos.getIntento());
			regIntentosJson.put("sent", regIntentos.getSent());
			respuesta.put("regIntentos", regIntentosJson);
			// TODO validar si es el 3er intento y es fallido
			is3erFaill(regIntentos.getIntento(), errorCode, validateIneRequest.getCurp(),
					validateIneRequest.getIdOperation());
			log.info("fin de la validacion ---------------->" + respuesta.toString());
			return new ResponseEntity<>(respuesta.toString(), HttpStatus.OK);
		} else if (res.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
			dataRequest.put("errorCode", "02");
			dataRequest.put("serviceCode", res.getStatusCodeValue());
			dataRequest.put("errorMessage", res.getBody());
		} else if (res.getStatusCode().is4xxClientError()) {
			if(res.getHeaders().containsKey("exception")) {
				String exep = res.getHeaders().getFirst("exception");
				log.info("%%%%%%%%%%% Exeption"+exep);
				dataRequest.put("errorCode", "05");
			}else {
				dataRequest.put("errorCode", "03");
				log.info("%%%%%%%%%%% NO SE PUDO OBTENER EL VALOR Exeption");
			}
			dataRequest.put("serviceCode", res.getStatusCodeValue());
			dataRequest.put("errorMessage", res.getBody());
		} else {
			dataRequest.put("errorCode", "09");
			dataRequest.put("serviceCode", res.getStatusCodeValue());
			dataRequest.put("errorMessage", res.getBody());
		}
		regIntentos.setDataRequest(dataRequest.toString());
		regIntentos.setIntento(numIntentoPrev);// no se ejecuto el llamado
		regIntentos.setReginfo(false);// no se ejecuto el llamado
		regIntentos.setRegbiom(false);// no se ejecuto el llamado
		regIntentos.setSent(false);// no se ejecuto el llamado
		regIntentos = bidClieRegConsultIneRepository.save(regIntentos);
		log.info("INE ValidateIneRequest End----->>> ERROR:" + res.getBody());
		return new ResponseEntity<>(dataRequest.toString(), HttpStatus.OK);

	}

	/**
	 * Validar si es el 3er intento y es fallido
	 * 
	 * @param intento
	 * @param errorCode
	 * @param regIntentos
	 */
	@Transactional
	private void is3erFaill(int intento, String errorCode, String curp, Long idClie) {
		if (intento >= 3 && !errorCode.equals("00")) {// tercer intento fallido
			try {
				log.info("INFO: el usuario {} ha llegado al limite de intentos posibles, intente con otro documento.");
//			y se borra el curp					
				BidClieCurp bdCurp = bidCurpRepository.findFirstByCurp(curp);
				if (bdCurp.getIdTipo() != 4) {// evaluar tipo 4 y no borrar
					BidClieCurp bdCurpPersist = new BidClieCurp();
					bdCurpPersist.setIdClie(bdCurp.getIdClie());
					bdCurpPersist.setCurp(bdCurp.getCurp());
					bdCurpPersist.setIdTipo(bdCurp.getIdTipo());
					bdCurpPersist.setIdEsta(bdCurp.getIdEsta());
					bdCurpPersist.setRel(bdCurp.getRel());
					bdCurpPersist.setFchCrea(bdCurp.getFchCrea());
					bdCurpPersist.setFchModi(new Timestamp(System.currentTimeMillis()));
					log.info("> Borrando Curp: " + bdCurpPersist.getCurp() + "     idClie: "
							+ bdCurpPersist.getIdClie());
					try {
						bidCurpRepository.delete(bdCurpPersist);
					} catch (Exception e2) {
						log.error("ERROR: error al borrar el curp delete ", e2);
						try {
							if (!bdCurpPersist.getCurp().contains("_" + idClie)) {
								bidCurpRepository.updateCurp(bdCurpPersist.getCurp() + "_" + idClie,
										bdCurpPersist.getIdClie());
							} else {
								log.info("INFO: El curp ya fue utilizado e imvalidado anteriormente");
							}
						} catch (Exception e) {
							log.error("ERROR: error al borrar el curp updateCurp", e);
						}
					}
				}
//			y idclie 	
//				BidClie clie = bidClieRepository.findOne(bdCurp.getIdClie());
//				log.info("> Borrando clie: "+ clie.getIdClie());
//				bidClieRepository.delete(clie);
//			reinicia el contador 				
//				List<BidClieRegConsultIne> intentos = bidClieRegConsultIneRepository.findByidClie(idClie);
//				if(intentos!=null && !intentos.isEmpty()) {
//					bidClieRegConsultIneRepository.delete(intentos.get(0));
//				}
				log.info("INFO: curp borrado.");
			} catch (Exception e) {
				// TODO: handle exception
				log.error("ERROR: error al borrar el curp ", e);
			}
		}
	}

	@Transactional
	@RequestMapping(value = "/logCurp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> logCurp(@RequestBody String req) {
		log.info("INFO: logCurp   ");
		JSONObject request = new JSONObject(req);
		Long idClie = Long.valueOf(request.optString("idClie"));
		BidClieCurp bdCurpPersist = bidCurpRepository.findTopByIdClie(idClie);
		JSONObject response = new JSONObject();
		try {
			if (!bdCurpPersist.getCurp().contains("_" + idClie)) {
				bidCurpRepository.updateCurp(bdCurpPersist.getCurp() + "_" + idClie, bdCurpPersist.getIdClie());
			} else {
				log.info("INFO: El curp ya fue utilizado e imvalidado anteriormente");
			}
			log.info("INFO: currp updated satisfactoriament.");
			response.put("response", "OK");
			return new ResponseEntity<>(response.toString(), HttpStatus.OK);
		} catch (Exception e) {
			log.error("ERROR: error al borrar el curp updateCurp", e);
			response.put("response", "OK");
			return new ResponseEntity<>(response.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private ValidateIneRequest validateIneMapper(String request) {
		JSONObject jRequest = new JSONObject(request);
//		log.info(">" + LogUtil.logJsonObject(jRequest));
		ValidateIneRequest validateIneRequest = new ValidateIneRequest();

		validateIneRequest.setRestart(jRequest.optBoolean("restart"));
		validateIneRequest.setIdOperation(jRequest.optLong("idOperation"));
		validateIneRequest.setOcr((float) jRequest.optDouble("ocr"));
		validateIneRequest.setCic((float) jRequest.optDouble("cic"));
		validateIneRequest.setName(jRequest.optString("name"));
		validateIneRequest.setFatherLastName(jRequest.optString("fatherLastName"));
		validateIneRequest.setMotherLastName(jRequest.optString("motherLastName"));
		validateIneRequest.setRegistryYear(jRequest.optString("registryYear"));
		validateIneRequest.setEmissionYear(jRequest.optString("emissionYear"));
		validateIneRequest.setElectorCode(jRequest.optString("electorCode"));
		validateIneRequest.setCurp(jRequest.optString("curp"));
		validateIneRequest.setEmissionNumber(jRequest.optString("emissionNumber"));
		JSONObject others = jRequest.optJSONObject("others");
		Others othersObject = new Others();
		othersObject.setConsent(others.optBoolean("consent"));
		JSONObject locationObject = others.optJSONObject("location");
		com.teknei.bid.service.ine.request.Location location = new com.teknei.bid.service.ine.request.Location();
		location.setLongitude((float) locationObject.optDouble("longitude"));
		location.setLatitude((float) locationObject.optDouble("latitude"));
		location.setCity(locationObject.optString("city"));
		location.setState(locationObject.optString("state"));
		location.setZipCode(locationObject.optString("zipCode"));
		othersObject.setLocationObject(location);
		List<Biometric> bimetricList = new ArrayList<>();
		JSONArray biomlist = others.getJSONArray("biometrics");
		
		for (int i=0; i<biomlist.length(); i++) {
			log.info(biomlist.getJSONObject(i).optString("fingerNumber"));
//		    JSONObject item = biometrics.getJSONObject(i);
			if (!biomlist.getJSONObject(i).optString("fingerPrint").isEmpty()) {
				log.info("fingerNumber");
				log.info(biomlist.getJSONObject(i).optString("fingerNumber"));
				JSONObject b = biomlist.optJSONObject(i);
				Biometric biometric = new Biometric();
				biometric.setType(b.optString("type"));
				biometric.setFingerNumber(b.optString("fingerNumber"));
				biometric.setFingerPrint(b.optString("fingerPrint"));
				bimetricList.add(biometric);
			}
		}
		
		
//		JSONObject b1 = biomlist.optJSONObject(0);
//		Biometric biometric = new Biometric();
//		biometric.setType(b1.optString("type"));
//		biometric.setFingerNumber(b1.optString("fingerNumber"));
//		biometric.setFingerPrint(b1.optString("fingerPrint"));
//		bimetricList.add(biometric);
//		
//		JSONObject b2 = biomlist.optJSONObject(1);
//		biometric = new Biometric();
//		biometric.setType(b2.optString("type"));
//		biometric.setFingerNumber(b2.optString("fingerNumber"));
//		biometric.setFingerPrint(b2.optString("fingerPrint"));
//		bimetricList.add(biometric);
//		
//		
//		if (biomlist.getJSONObject(2) != null) {
//			JSONObject b = new JSONObject();
//			b = biomlist.optJSONObject(2);
//			biometric = new Biometric();
//			biometric.setType(b.optString("type"));
//			biometric.setFingerNumber(b.optString("fingerNumber"));
//			biometric.setFingerPrint(b.optString("fingerPrint"));
//			bimetricList.add(biometric);
//		}
//		if (biomlist.getJSONObject(3) != null) {
//			JSONObject b = new JSONObject();
//			b = biomlist.optJSONObject(3);
//			log.info("biomlist.getJSONObject(3) ->" + b);
//			biometric = new Biometric();
//			biometric.setType(b.optString("type"));
//			biometric.setFingerNumber(b.optString("fingerNumber"));
//			biometric.setFingerPrint(b.optString("fingerPrint"));
//			bimetricList.add(biometric);
//		}
//		if (biomlist.getJSONObject(4) != null) {
//			JSONObject b = new JSONObject();
//			b = biomlist.optJSONObject(4);
//			biometric = new Biometric();
//			biometric.setType(b.optString("type"));
//			biometric.setFingerNumber(b.optString("fingerNumber"));
//			biometric.setFingerPrint(b.optString("fingerPrint"));
//			bimetricList.add(biometric);
//		}
//		if (biomlist.getJSONObject(5) != null) {
//			JSONObject b = new JSONObject();
//			b = biomlist.optJSONObject(5);
//			biometric = new Biometric();
//			biometric.setType(b.optString("type"));
//			biometric.setFingerNumber(b.optString("fingerNumber"));
//			biometric.setFingerPrint(b.optString("fingerPrint"));
//			bimetricList.add(biometric);
//		}
//		if (biomlist.getJSONObject(6) != null) {
//			JSONObject b = new JSONObject();
//			b = biomlist.optJSONObject(6);
//			biometric = new Biometric();
//			biometric.setType(b.optString("type"));
//			biometric.setFingerNumber(b.optString("fingerNumber"));
//			biometric.setFingerPrint(b.optString("fingerPrint"));
//			bimetricList.add(biometric);
//		}
//		if (biomlist.getJSONObject(7) != null) {
//			JSONObject b = new JSONObject();
//			b = biomlist.optJSONObject(7);
//			biometric = new Biometric();
//			biometric.setType(b.optString("type"));
//			biometric.setFingerNumber(b.optString("fingerNumber"));
//			biometric.setFingerPrint(b.optString("fingerPrint"));
//			bimetricList.add(biometric);
//		}
//		if (biomlist.getJSONObject(8) != null) {
//			JSONObject b = new JSONObject();
//			b = biomlist.optJSONObject(8);
//			biometric = new Biometric();
//			biometric.setType(b.optString("type"));
//			biometric.setFingerNumber(b.optString("fingerNumber"));
//			biometric.setFingerPrint(b.optString("fingerPrint"));
//			bimetricList.add(biometric);
//		}
		
		othersObject.setBimetric(bimetricList);
		validateIneRequest.setOthersObject(othersObject);
		return validateIneRequest;
	}

}
