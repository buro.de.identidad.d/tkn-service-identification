package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CurpValidateByCustomerData implements Serializable {

    private Long idCustomer;

}