package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RequestEncFilesDTO implements Serializable {

    private Long operationId;
    private String b64Anverse;
    private String b64Reverse;
    private String contentType;
    private String obs;
    private String username;

}