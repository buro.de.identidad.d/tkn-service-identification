package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CurpRequestByCustomerData implements Serializable {

    private Long idCustomer;
    private String entidad;

}