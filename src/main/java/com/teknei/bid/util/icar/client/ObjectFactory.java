
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.teknei.bidserver.client package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.teknei.bidserver.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DsDocumentCheckIn }
     * 
     */
    public DsDocumentCheckIn createDsDocumentCheckIn() {
        return new DsDocumentCheckIn();
    }

    /**
     * Create an instance of {@link AnalyzeDocumentResponse }
     * 
     */
    public AnalyzeDocumentResponse createAnalyzeDocumentResponse() {
        return new AnalyzeDocumentResponse();
    }

    /**
     * Create an instance of {@link AnalyzeDocument }
     * 
     */
    public AnalyzeDocument createAnalyzeDocument() {
        return new AnalyzeDocument();
    }

    /**
     * Create an instance of {@link DsDocumentCheckOut }
     * 
     */
    public DsDocumentCheckOut createDsDocumentCheckOut() {
        return new DsDocumentCheckOut();
    }

    /**
     * Create an instance of {@link DsDocumentCheckOut.DocumentCheck }
     * 
     */
    public DsDocumentCheckOut.DocumentCheck createDsDocumentCheckOutDocumentCheck() {
        return new DsDocumentCheckOut.DocumentCheck();
    }

    /**
     * Create an instance of {@link AnalyzeDocumentV2 }
     * 
     */
    public AnalyzeDocumentV2 createAnalyzeDocumentV2() {
        return new AnalyzeDocumentV2();
    }

    /**
     * Create an instance of {@link DocumentCheckInV2 }
     * 
     */
    public DocumentCheckInV2 createDocumentCheckInV2() {
        return new DocumentCheckInV2();
    }

    /**
     * Create an instance of {@link AnalyzeDocumentV2Response }
     * 
     */
    public AnalyzeDocumentV2Response createAnalyzeDocumentV2Response() {
        return new AnalyzeDocumentV2Response();
    }

    /**
     * Create an instance of {@link DocumentCheckOutV2 }
     * 
     */
    public DocumentCheckOutV2 createDocumentCheckOutV2() {
        return new DocumentCheckOutV2();
    }

    /**
     * Create an instance of {@link DsDocumentCheckIn.DocumentCheck }
     * 
     */
    public DsDocumentCheckIn.DocumentCheck createDsDocumentCheckInDocumentCheck() {
        return new DsDocumentCheckIn.DocumentCheck();
    }

    /**
     * Create an instance of {@link AnalyzeDocumentV2ExResponse }
     * 
     */
    public AnalyzeDocumentV2ExResponse createAnalyzeDocumentV2ExResponse() {
        return new AnalyzeDocumentV2ExResponse();
    }

    /**
     * Create an instance of {@link AnalyzeDocumentV2Ex }
     * 
     */
    public AnalyzeDocumentV2Ex createAnalyzeDocumentV2Ex() {
        return new AnalyzeDocumentV2Ex();
    }

    /**
     * Create an instance of {@link AnalyzeDocumentResponse.AnalyzeDocumentResult }
     * 
     */
    public AnalyzeDocumentResponse.AnalyzeDocumentResult createAnalyzeDocumentResponseAnalyzeDocumentResult() {
        return new AnalyzeDocumentResponse.AnalyzeDocumentResult();
    }

    /**
     * Create an instance of {@link AnalyzeDocument.ADsIn }
     * 
     */
    public AnalyzeDocument.ADsIn createAnalyzeDocumentADsIn() {
        return new AnalyzeDocument.ADsIn();
    }

    /**
     * Create an instance of {@link Field }
     * 
     */
    public Field createField() {
        return new Field();
    }

    /**
     * Create an instance of {@link ArrayOfField }
     * 
     */
    public ArrayOfField createArrayOfField() {
        return new ArrayOfField();
    }

    /**
     * Create an instance of {@link Message }
     *
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link DocumentImage }
     * 
     */
    public DocumentImage createDocumentImage() {
        return new DocumentImage();
    }

    /**
     * Create an instance of {@link ArrayOfMessage }
     * 
     */
    public ArrayOfMessage createArrayOfMessage() {
        return new ArrayOfMessage();
    }

    /**
     * Create an instance of {@link DsDocumentCheckOut.Message }
     * 
     */
    public DsDocumentCheckOut.Message createDsDocumentCheckOutMessage() {
        return new DsDocumentCheckOut.Message();
    }

    /**
     * Create an instance of {@link DsDocumentCheckOut.DocumentCheck.Properties }
     * 
     */
    public DsDocumentCheckOut.DocumentCheck.Properties createDsDocumentCheckOutDocumentCheckProperties() {
        return new DsDocumentCheckOut.DocumentCheck.Properties();
    }

}
