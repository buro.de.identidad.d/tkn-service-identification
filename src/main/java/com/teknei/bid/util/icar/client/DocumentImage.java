
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocumentImage complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentImage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ImageResolution" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Image" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="Filetype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DeviceInfo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentImage", propOrder = {
    "imageResolution",
    "image",
    "filetype",
    "deviceInfo"
})
public class DocumentImage {

    @XmlElement(name = "ImageResolution")
    protected int imageResolution;
    @XmlElement(name = "Image", required = true, nillable = true)
    protected byte[] image;
    @XmlElement(name = "Filetype", required = true, nillable = true)
    protected String filetype;
    @XmlElement(name = "DeviceInfo")
    protected byte[] deviceInfo;

    /**
     * Obtiene el valor de la propiedad imageResolution.
     * 
     */
    public int getImageResolution() {
        return imageResolution;
    }

    /**
     * Define el valor de la propiedad imageResolution.
     * 
     */
    public void setImageResolution(int value) {
        this.imageResolution = value;
    }

    /**
     * Obtiene el valor de la propiedad image.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Define el valor de la propiedad image.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setImage(byte[] value) {
        this.image = value;
    }

    /**
     * Obtiene el valor de la propiedad filetype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiletype() {
        return filetype;
    }

    /**
     * Define el valor de la propiedad filetype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiletype(String value) {
        this.filetype = value;
    }

    /**
     * Obtiene el valor de la propiedad deviceInfo.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getDeviceInfo() {
        return deviceInfo;
    }

    /**
     * Define el valor de la propiedad deviceInfo.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setDeviceInfo(byte[] value) {
        this.deviceInfo = value;
    }

}
