/**
 * icarSAAS Web Service Public
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://IdCloud.iCarVision/WS/", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package com.teknei.bid.util.icar.client;
