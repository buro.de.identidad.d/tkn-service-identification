
package com.teknei.bid.util.icar.client;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "WsDocumentSoap", targetNamespace = "http://IdCloud.iCarVision/WS/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface WsDocumentSoap {


    /**
     * Method that analyzes and verifies a document
     * 
     * @param aPwd
     * @param aDsIn
     * @param aUser
     * @return
     *     returns com.teknei.bidserver.client.AnalyzeDocumentResponse.AnalyzeDocumentResult
     */
    @WebMethod(operationName = "AnalyzeDocument", action = "http://IdCloud.iCarVision/WS/AnalyzeDocument")
    @WebResult(name = "AnalyzeDocumentResult", targetNamespace = "http://IdCloud.iCarVision/WS/")
    @RequestWrapper(localName = "AnalyzeDocument", targetNamespace = "http://IdCloud.iCarVision/WS/", className = "com.teknei.bidserver.client.AnalyzeDocument")
    @ResponseWrapper(localName = "AnalyzeDocumentResponse", targetNamespace = "http://IdCloud.iCarVision/WS/", className = "com.teknei.bidserver.client.AnalyzeDocumentResponse")
    public com.teknei.bid.util.icar.client.AnalyzeDocumentResponse.AnalyzeDocumentResult analyzeDocument(
            @WebParam(name = "aUser", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    String aUser,
            @WebParam(name = "aPwd", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    String aPwd,
            @WebParam(name = "aDsIn", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    com.teknei.bid.util.icar.client.AnalyzeDocument.ADsIn aDsIn);

    /**
     * Method that analyzes and verifies a document V2
     *
     * @param getDocumentIdForMerging
     * @param docInV2
     * @param company
     * @param pwd
     * @param user
     * @return
     *     returns com.teknei.bidserver.client.DocumentCheckOutV2
     */
    @WebMethod(operationName = "AnalyzeDocumentV2", action = "http://IdCloud.iCarVision/WS/AnalyzeDocumentV2")
    @WebResult(name = "AnalyzeDocumentV2Result", targetNamespace = "http://IdCloud.iCarVision/WS/")
    @RequestWrapper(localName = "AnalyzeDocumentV2", targetNamespace = "http://IdCloud.iCarVision/WS/", className = "com.teknei.bidserver.client.AnalyzeDocumentV2")
    @ResponseWrapper(localName = "AnalyzeDocumentV2Response", targetNamespace = "http://IdCloud.iCarVision/WS/", className = "com.teknei.bidserver.client.AnalyzeDocumentV2Response")
    public DocumentCheckOutV2 analyzeDocumentV2(
            @WebParam(name = "Company", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    String company,
            @WebParam(name = "User", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    String user,
            @WebParam(name = "Pwd", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    String pwd,
            @WebParam(name = "GetDocumentIdForMerging", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    boolean getDocumentIdForMerging,
            @WebParam(name = "DocInV2", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    DocumentCheckInV2 docInV2);

    /**
     * Method that analyzes and verifies a document V2 IDBoxInfo
     *
     * @param docInV2Ex
     * @param getDocumentIdForMerging
     * @param company
     * @param pwd
     * @param user
     * @return
     *     returns com.teknei.bidserver.client.DocumentCheckOutV2
     */
    @WebMethod(operationName = "AnalyzeDocumentV2Ex", action = "http://IdCloud.iCarVision/WS/AnalyzeDocumentV2Ex")
    @WebResult(name = "AnalyzeDocumentV2ExResult", targetNamespace = "http://IdCloud.iCarVision/WS/")
    @RequestWrapper(localName = "AnalyzeDocumentV2Ex", targetNamespace = "http://IdCloud.iCarVision/WS/", className = "com.teknei.bidserver.client.AnalyzeDocumentV2Ex")
    @ResponseWrapper(localName = "AnalyzeDocumentV2ExResponse", targetNamespace = "http://IdCloud.iCarVision/WS/", className = "com.teknei.bidserver.client.AnalyzeDocumentV2ExResponse")
    public DocumentCheckOutV2 analyzeDocumentV2Ex(
            @WebParam(name = "Company", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    String company,
            @WebParam(name = "User", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    String user,
            @WebParam(name = "Pwd", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    String pwd,
            @WebParam(name = "GetDocumentIdForMerging", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    boolean getDocumentIdForMerging,
            @WebParam(name = "DocInV2Ex", targetNamespace = "http://IdCloud.iCarVision/WS/")
                    DocumentCheckInV2 docInV2Ex);

}
