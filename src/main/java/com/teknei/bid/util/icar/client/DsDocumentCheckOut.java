
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="Message">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DocumentCheck">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Image1cut" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                   &lt;element name="Image2cut" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                   &lt;element name="ImagePhoto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                   &lt;element name="ImageSignature" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                   &lt;element name="ImageFingerPrint" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                   &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DocumentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Expeditor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Expiry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Properties" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "messageOrDocumentCheck"
})
@XmlRootElement(name = "DsDocumentCheckOut", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
public class DsDocumentCheckOut {

    @XmlElements({
        @XmlElement(name = "Message", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", type = Message.class),
        @XmlElement(name = "DocumentCheck", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", type = DocumentCheck.class)
    })
    protected List<Object> messageOrDocumentCheck;

    /**
     * Gets the value of the messageOrDocumentCheck property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messageOrDocumentCheck property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessageOrDocumentCheck().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Message }
     * {@link DocumentCheck }
     *
     *
     */
    public List<Object> getMessageOrDocumentCheck() {
        if (messageOrDocumentCheck == null) {
            messageOrDocumentCheck = new ArrayList<Object>();
        }
        return this.messageOrDocumentCheck;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     *
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Image1cut" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *         &lt;element name="Image2cut" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *         &lt;element name="ImagePhoto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *         &lt;element name="ImageSignature" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *         &lt;element name="ImageFingerPrint" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *         &lt;element name="Result" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Warning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DocumentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Expeditor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Nationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Expiry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Properties" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference",
        "image1Cut",
        "image2Cut",
        "imagePhoto",
        "imageSignature",
        "imageFingerPrint",
        "result",
        "warning",
        "documentType",
        "expeditor",
        "documentNumber",
        "name",
        "surname",
        "nationality",
        "gender",
        "birthday",
        "expiry",
        "properties"
    })
    public static class DocumentCheck {

        @XmlElement(name = "Reference", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", required = true)
        protected String reference;
        @XmlElement(name = "Image1cut", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected byte[] image1Cut;
        @XmlElement(name = "Image2cut", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected byte[] image2Cut;
        @XmlElement(name = "ImagePhoto", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected byte[] imagePhoto;
        @XmlElement(name = "ImageSignature", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected byte[] imageSignature;
        @XmlElement(name = "ImageFingerPrint", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected byte[] imageFingerPrint;
        @XmlElement(name = "Result", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", required = true)
        protected String result;
        @XmlElement(name = "Warning", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String warning;
        @XmlElement(name = "DocumentType", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String documentType;
        @XmlElement(name = "Expeditor", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String expeditor;
        @XmlElement(name = "DocumentNumber", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String documentNumber;
        @XmlElement(name = "Name", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String name;
        @XmlElement(name = "Surname", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String surname;
        @XmlElement(name = "Nationality", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String nationality;
        @XmlElement(name = "Gender", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String gender;
        @XmlElement(name = "Birthday", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String birthday;
        @XmlElement(name = "Expiry", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected String expiry;
        @XmlElement(name = "Properties", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected List<Properties> properties;

        /**
         * Obtiene el valor de la propiedad reference.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getReference() {
            return reference;
        }

        /**
         * Define el valor de la propiedad reference.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setReference(String value) {
            this.reference = value;
        }

        /**
         * Obtiene el valor de la propiedad image1Cut.
         *
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getImage1Cut() {
            return image1Cut;
        }

        /**
         * Define el valor de la propiedad image1Cut.
         *
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setImage1Cut(byte[] value) {
            this.image1Cut = value;
        }

        /**
         * Obtiene el valor de la propiedad image2Cut.
         *
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getImage2Cut() {
            return image2Cut;
        }

        /**
         * Define el valor de la propiedad image2Cut.
         *
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setImage2Cut(byte[] value) {
            this.image2Cut = value;
        }

        /**
         * Obtiene el valor de la propiedad imagePhoto.
         *
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getImagePhoto() {
            return imagePhoto;
        }

        /**
         * Define el valor de la propiedad imagePhoto.
         *
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setImagePhoto(byte[] value) {
            this.imagePhoto = value;
        }

        /**
         * Obtiene el valor de la propiedad imageSignature.
         *
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getImageSignature() {
            return imageSignature;
        }

        /**
         * Define el valor de la propiedad imageSignature.
         *
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setImageSignature(byte[] value) {
            this.imageSignature = value;
        }

        /**
         * Obtiene el valor de la propiedad imageFingerPrint.
         *
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getImageFingerPrint() {
            return imageFingerPrint;
        }

        /**
         * Define el valor de la propiedad imageFingerPrint.
         *
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setImageFingerPrint(byte[] value) {
            this.imageFingerPrint = value;
        }

        /**
         * Obtiene el valor de la propiedad result.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getResult() {
            return result;
        }

        /**
         * Define el valor de la propiedad result.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setResult(String value) {
            this.result = value;
        }

        /**
         * Obtiene el valor de la propiedad warning.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getWarning() {
            return warning;
        }

        /**
         * Define el valor de la propiedad warning.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setWarning(String value) {
            this.warning = value;
        }

        /**
         * Obtiene el valor de la propiedad documentType.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDocumentType() {
            return documentType;
        }

        /**
         * Define el valor de la propiedad documentType.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDocumentType(String value) {
            this.documentType = value;
        }

        /**
         * Obtiene el valor de la propiedad expeditor.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getExpeditor() {
            return expeditor;
        }

        /**
         * Define el valor de la propiedad expeditor.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setExpeditor(String value) {
            this.expeditor = value;
        }

        /**
         * Obtiene el valor de la propiedad documentNumber.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDocumentNumber() {
            return documentNumber;
        }

        /**
         * Define el valor de la propiedad documentNumber.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDocumentNumber(String value) {
            this.documentNumber = value;
        }

        /**
         * Obtiene el valor de la propiedad name.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getName() {
            return name;
        }

        /**
         * Define el valor de la propiedad name.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtiene el valor de la propiedad surname.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSurname() {
            return surname;
        }

        /**
         * Define el valor de la propiedad surname.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSurname(String value) {
            this.surname = value;
        }

        /**
         * Obtiene el valor de la propiedad nationality.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getNationality() {
            return nationality;
        }

        /**
         * Define el valor de la propiedad nationality.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setNationality(String value) {
            this.nationality = value;
        }

        /**
         * Obtiene el valor de la propiedad gender.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getGender() {
            return gender;
        }

        /**
         * Define el valor de la propiedad gender.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setGender(String value) {
            this.gender = value;
        }

        /**
         * Obtiene el valor de la propiedad birthday.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getBirthday() {
            return birthday;
        }

        /**
         * Define el valor de la propiedad birthday.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setBirthday(String value) {
            this.birthday = value;
        }

        /**
         * Obtiene el valor de la propiedad expiry.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getExpiry() {
            return expiry;
        }

        /**
         * Define el valor de la propiedad expiry.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setExpiry(String value) {
            this.expiry = value;
        }

        /**
         * Gets the value of the properties property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the properties property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProperties().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Properties }
         *
         *
         */
        public List<Properties> getProperties() {
            if (properties == null) {
                properties = new ArrayList<Properties>();
            }
            return this.properties;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type",
            "code",
            "description",
            "value"
        })
        public static class Properties {

            @XmlElement(name = "Type", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", required = true)
            protected String type;
            @XmlElement(name = "Code", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", required = true)
            protected String code;
            @XmlElement(name = "Description", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", required = true)
            protected String description;
            @XmlElement(name = "Value", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", required = true)
            protected String value;

            /**
             * Obtiene el valor de la propiedad type.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getType() {
                return type;
            }

            /**
             * Define el valor de la propiedad type.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setType(String value) {
                this.type = value;
            }

            /**
             * Obtiene el valor de la propiedad code.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCode() {
                return code;
            }

            /**
             * Define el valor de la propiedad code.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCode(String value) {
                this.code = value;
            }

            /**
             * Obtiene el valor de la propiedad description.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDescription() {
                return description;
            }

            /**
             * Define el valor de la propiedad description.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDescription(String value) {
                this.description = value;
            }

            /**
             * Obtiene el valor de la propiedad value.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getValue() {
                return value;
            }

            /**
             * Define el valor de la propiedad value.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setValue(String value) {
                this.value = value;
            }

        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "description",
        "code"
    })
    public static class Message {

        @XmlElement(name = "Description", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd", required = true)
        protected String description;
        @XmlElement(name = "Code", namespace = "http://tempuri.org/DsDocumentCheckOut.xsd")
        protected Integer code;

        /**
         * Obtiene el valor de la propiedad description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Define el valor de la propiedad description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Obtiene el valor de la propiedad code.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCode() {
            return code;
        }

        /**
         * Define el valor de la propiedad code.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCode(Integer value) {
            this.code = value;
        }

    }

}
