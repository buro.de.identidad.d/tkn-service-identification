
package com.teknei.bid.util.icar.client;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="DocumentCheck">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ImageResolution" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Image1" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="Image2" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "documentCheck"
})
@XmlRootElement(name = "DsDocumentCheckIn")
public class DsDocumentCheckIn {

    @XmlElement(name = "DocumentCheck")
    protected List<DocumentCheck> documentCheck;

    /**
     * Gets the value of the documentCheck property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentCheck property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentCheck().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentCheck }
     *
     *
     */
    public List<DocumentCheck> getDocumentCheck() {
        if (documentCheck == null) {
            documentCheck = new ArrayList<DocumentCheck>();
        }
        return this.documentCheck;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Reference" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ImageResolution" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Image1" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="Image2" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference",
        "activity",
        "imageResolution",
        "image1",
        "image2"
    })
    public static class DocumentCheck {

        @XmlElement(name = "Reference", required = true)
        protected String reference;
        @XmlElement(name = "Activity")
        protected String activity;
        @XmlElement(name = "ImageResolution")
        protected int imageResolution;
        @XmlElement(name = "Image1", required = true)
        protected byte[] image1;
        @XmlElement(name = "Image2")
        protected byte[] image2;

        /**
         * Obtiene el valor de la propiedad reference.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReference() {
            return reference;
        }

        /**
         * Define el valor de la propiedad reference.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReference(String value) {
            this.reference = value;
        }

        /**
         * Obtiene el valor de la propiedad activity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActivity() {
            return activity;
        }

        /**
         * Define el valor de la propiedad activity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActivity(String value) {
            this.activity = value;
        }

        /**
         * Obtiene el valor de la propiedad imageResolution.
         * 
         */
        public int getImageResolution() {
            return imageResolution;
        }

        /**
         * Define el valor de la propiedad imageResolution.
         * 
         */
        public void setImageResolution(int value) {
            this.imageResolution = value;
        }

        /**
         * Obtiene el valor de la propiedad image1.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getImage1() {
            return image1;
        }

        /**
         * Define el valor de la propiedad image1.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setImage1(byte[] value) {
            this.image1 = value;
        }

        /**
         * Obtiene el valor de la propiedad image2.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getImage2() {
            return image2;
        }

        /**
         * Define el valor de la propiedad image2.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setImage2(byte[] value) {
            this.image2 = value;
        }

    }

}
