package com.teknei.bid.util.cecoban;

import com.teknei.bid.dto.PersonDataIneTKNRequest;
import com.teknei.bid.persistence.entities.BidClieIfeIne;
import com.teknei.bid.persistence.entities.BidClieIfeInePK;
import com.teknei.bid.persistence.repository.BidIfeRepository;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

@Service
public class CecobanUtil {

    @Autowired
    private BidIfeRepository bidIfeRepository;
    @Value("${tkn.cecoban.institucionId}")
    private String institutionId;
    @Value("${tkn.cecoban.folioId}")
    private String folioId;
    @Value("${tkn.cecoban.latitud}")
    private Double latitud;
    @Value("${tkn.cecoban.longitud}")
    private Double longitud;
    @Value("${tkn.cecoban.cp}")
    private String cp;
    @Value("${tkn.cecoban.alto}")
    private Integer alto;
    @Value("${tkn.cecoban.ancho}")
    private Integer ancho;
    @Value("${tkn.cecoban.tipoFinger}")
    private String tipoFinger;
    @Value("${tkn.cecoban.url}")
    private String cecobanIp;

    private static final Logger log = LoggerFactory.getLogger(CecobanUtil.class);

    public JSONObject makeRequest(PersonDataIneTKNRequest personData) throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException, IOException {
        BidClieIfeInePK pk = new BidClieIfeInePK();
        pk.setIdClie(personData.getId());
        pk.setIdIfe(personData.getId());
        BidClieIfeIne bidClieIfeIne = bidIfeRepository.findOne(pk);
        JSONObject requestMain = new JSONObject();
        JSONObject requestHeader = new JSONObject();//Encabezado
        requestHeader.put("SolicitudId", String.valueOf(bidIfeRepository.getCecobanId()));
        requestHeader.put("InstitucionId", institutionId);
        requestHeader.put("FolioCliente", folioId);
        requestMain.put("Encabezado", requestHeader);
        JSONObject requestData = new JSONObject();//Datos
        requestData.put("OCR", bidClieIfeIne.getOcr());
        requestData.put("CIC", "");
        requestData.put("Nombre", bidClieIfeIne.getNomb());
        requestData.put("ApellidoPaterno", bidClieIfeIne.getApePate());
        requestData.put("ApellidoMaterno", bidClieIfeIne.getApeMate());
        requestData.put("CURP", bidClieIfeIne.getCurp());
        requestData.put("Consentimiento", 1);
        requestMain.put("Datos", requestData);
        JSONObject requestPosSat = new JSONObject();
        requestPosSat.put("Latitud", latitud);
        requestPosSat.put("Longitud", longitud);
        JSONObject requestCp = new JSONObject();
        requestCp.put("CodigoPostal", cp);
        requestCp.put("Ciudad", "");
        requestCp.put("Estado", "");
        JSONObject requestUb = new JSONObject();
        requestUb.put("PosicionSatelital", requestPosSat);
        requestUb.put("Localidad", requestCp);
        requestMain.put("Ubicacion", requestUb);
        JSONObject requestFingerRight = new JSONObject();
        requestFingerRight.put("Alto", alto);
        requestFingerRight.put("Ancho", ancho);
        requestFingerRight.put("Tipo", tipoFinger);
        requestFingerRight.put("Nombre", "Huella2");
        requestFingerRight.put("Minucia", personData.getRightIndexB64());
        JSONArray requestMinArray = new JSONArray();
        requestMinArray.put(requestFingerRight);
        requestMain.put("Minucias", requestMinArray);
        /*
        CloseableHttpClient httpClient
                = HttpClients.custom()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> en = new HttpEntity<>(requestMain.toString(), headers);
        ResponseEntity<String> responseEntity
                = new RestTemplate(requestFactory).exchange(
                cecobanIp, HttpMethod.POST, en, String.class);

*/
        final SSLConnectionSocketFactory sslsf;
        try {
            sslsf = new SSLConnectionSocketFactory(SSLContext.getDefault(),
                    NoopHostnameVerifier.INSTANCE);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", new PlainConnectionSocketFactory())
                .register("https", sslsf)
                .build();

        final PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(registry);
        cm.setMaxTotal(100);
        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .setConnectionManager(cm)
                .build();
        HttpPost httpost = new HttpPost(cecobanIp);
        httpost.setHeader("Content-Type", "application/json");
        httpost.setEntity(new StringEntity(requestMain.toString()));
        CloseableHttpResponse httpResponse = httpClient.execute(httpost);
        String result = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
        JSONObject responseObject = new JSONObject(result);
        return responseObject;
    }

}